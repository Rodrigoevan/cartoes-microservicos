package br.com.mastertech.client.client.service;

import br.com.mastertech.client.client.exception.ClientNotFoundException;
import br.com.mastertech.client.client.model.Client;
import br.com.mastertech.client.client.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Client create(Client client) {
        return clientRepository.save(client);
    }

    public Client getById(Long id) {
        Optional<Client> byId = clientRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClientNotFoundException();
        }

        return byId.get();
    }

}

package br.com.mastertech.client.client.repository;

import br.com.mastertech.client.client.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

}

package br.com.mastertech.pagamento.pagamento.client;

import br.com.mastertech.pagamento.pagamento.exception.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PagamentoClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        System.out.println("response status = " + response.status());
        if(response.status() == 404) {
            return new CartaoNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}

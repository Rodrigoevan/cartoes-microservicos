package br.com.mastertech.pagamento.pagamento.client;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class PagamentoClientConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new PagamentoClientErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(PagamentoClientFallback::new)
                .build();
        return Resilience4jFeign.builder(decorators);
    }

}

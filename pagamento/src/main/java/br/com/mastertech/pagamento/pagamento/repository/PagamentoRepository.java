package br.com.mastertech.pagamento.pagamento.repository;

import br.com.mastertech.pagamento.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCartaoId(Long cartaoId);

}

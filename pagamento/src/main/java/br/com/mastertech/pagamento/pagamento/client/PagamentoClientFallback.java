package br.com.mastertech.pagamento.pagamento.client;

import br.com.mastertech.pagamento.pagamento.client.dto.create.CreateCartaoRequest;
import br.com.mastertech.pagamento.pagamento.client.dto.create.CreateCartaoResponse;
import br.com.mastertech.pagamento.pagamento.client.dto.get.GetCartaoResponse;
import br.com.mastertech.pagamento.pagamento.client.dto.update.UpdateCartaoRequest;
import br.com.mastertech.pagamento.pagamento.client.dto.update.UpdateCartaoResponse;
import br.com.mastertech.pagamento.pagamento.exception.CartaoNotFoundException;

import java.io.IOException;

public class PagamentoClientFallback implements CartaoClient {
    private Exception cause;

    PagamentoClientFallback(Exception cause) {
        this.cause = cause;
    }


    @Override
    public CreateCartaoResponse create(CreateCartaoRequest createCartaoRequest) {
        System.out.println(cause.getLocalizedMessage());
        System.out.println(cause.getClass().getName());
        if(cause instanceof IOException){
            throw new CartaoNotFoundException();
        }
        throw (RuntimeException) cause;
    }

    @Override
    public UpdateCartaoResponse update(String numero, UpdateCartaoRequest updateCartaoRequest) {
        System.out.println(cause.getLocalizedMessage());
        System.out.println(cause.getClass().getName());
        if(cause instanceof IOException){
            throw new CartaoNotFoundException();
        }
        throw (RuntimeException) cause;

    }

    @Override
    public GetCartaoResponse getByNumero(String numero) {

        System.out.println(cause.getLocalizedMessage());
        System.out.println(cause.getClass().getName());
        if(cause instanceof IOException){
            throw new CartaoNotFoundException();
        }
        throw (RuntimeException) cause;

    }

    @Override
    public GetCartaoResponse getById(Long id) {

        if(cause.getClass().getName().contains("CartaoNotFoundException")){
            throw new CartaoNotFoundException();
        }
        throw (RuntimeException) cause;

    }
}

package br.com.mastertech.pagamento.pagamento.controller;

import br.com.mastertech.pagamento.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.pagamento.model.dto.CreatePagamentoRequest;
import br.com.mastertech.pagamento.pagamento.model.dto.PagamentoResponse;
import br.com.mastertech.pagamento.pagamento.model.mapper.PagamentoMapper;
import br.com.mastertech.pagamento.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    public PagamentoResponse create(@RequestBody CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = pagamentoMapper.toPagamento(createPagamentoRequest);

        pagamento = pagamentoService.create(pagamento);

        return pagamentoMapper.toPagamentoResponse(pagamento);
    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<PagamentoResponse> listByCartao(@PathVariable Long cartaoId) {
        List<Pagamento> pagamentos = pagamentoService.listByCartao(cartaoId);
        return pagamentoMapper.toPagamentoResponse(pagamentos);
    }

}

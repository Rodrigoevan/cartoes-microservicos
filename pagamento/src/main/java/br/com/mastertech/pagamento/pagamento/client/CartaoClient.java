package br.com.mastertech.pagamento.pagamento.client;

import br.com.mastertech.pagamento.pagamento.client.dto.create.CreateCartaoRequest;
import br.com.mastertech.pagamento.pagamento.client.dto.create.CreateCartaoResponse;
import br.com.mastertech.pagamento.pagamento.client.dto.get.GetCartaoResponse;
import br.com.mastertech.pagamento.pagamento.client.dto.update.UpdateCartaoRequest;
import br.com.mastertech.pagamento.pagamento.client.dto.update.UpdateCartaoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;



@FeignClient(name = "cartao", configuration = PagamentoClientConfiguration.class)
public interface CartaoClient {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse create(@RequestBody CreateCartaoRequest createCartaoRequest);

    @PatchMapping("/cartao/{numero}")
    public UpdateCartaoResponse update(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest);

    @GetMapping("/cartao/{numero}")
    public GetCartaoResponse getByNumero(@PathVariable String numero);

    @GetMapping("/cartao/id/{id}")
    public GetCartaoResponse getById(@PathVariable Long id);

}

package br.com.mastertech.pagamento.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartão sem pagamentos")
public class CartaoSemPagamentos extends RuntimeException{
}

package br.com.mastertech.pagamento.pagamento.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePagamentoRequest {

    private String descricao;

    private Double valor;

    @JsonProperty("cartao_id")
    private Long cartaoId;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }
}

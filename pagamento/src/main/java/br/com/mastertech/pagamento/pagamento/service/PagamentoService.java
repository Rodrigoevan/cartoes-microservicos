package br.com.mastertech.pagamento.pagamento.service;

import br.com.mastertech.pagamento.pagamento.client.CartaoClient;
import br.com.mastertech.pagamento.pagamento.client.dto.get.GetCartaoResponse;
import br.com.mastertech.pagamento.pagamento.exception.CartaoSemPagamentos;
import br.com.mastertech.pagamento.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento create(Pagamento pagamento) {
        GetCartaoResponse cartao = cartaoClient.getById(pagamento.getCartaoId());

        pagamento.setCartaoId(cartao.getId());

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> listByCartao(Long cartaoId) {
        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(cartaoId);
        if (pagamentos.isEmpty()){
            throw new CartaoSemPagamentos();
        }
        return pagamentos;
    }

}

package br.com.mastertech.pagamento.pagamento.model.mapper;

import br.com.mastertech.pagamento.pagamento.client.CartaoClient;
import br.com.mastertech.pagamento.pagamento.client.dto.get.GetCartaoResponse;
import br.com.mastertech.pagamento.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.pagamento.model.dto.CreatePagamentoRequest;
import br.com.mastertech.pagamento.pagamento.model.dto.PagamentoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    @Autowired
    CartaoClient cartaoClient;

    public  PagamentoResponse toPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();

        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(pagamento.getCartaoId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());

        return pagamentoResponse;
    }

    public  List<PagamentoResponse> toPagamentoResponse(List<Pagamento> pagamentos) {
        List<PagamentoResponse> pagamentoResponses = new ArrayList<>();

        for (Pagamento pagamento : pagamentos) {
            pagamentoResponses.add(toPagamentoResponse(pagamento));
        }

        return pagamentoResponses;
    }

    public  Pagamento toPagamento(CreatePagamentoRequest createPagamentoRequest) {
        GetCartaoResponse cartao = cartaoClient.getById(createPagamentoRequest.getCartaoId());
        cartao.setId(createPagamentoRequest.getCartaoId());

        Pagamento pagamento = new Pagamento();

        pagamento.setDescricao(createPagamentoRequest.getDescricao());
        pagamento.setValor(createPagamentoRequest.getValor());
        pagamento.setCartaoId(cartao.getId());

        return pagamento;
    }
}

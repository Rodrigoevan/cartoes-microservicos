package br.com.mastertech.pagamento.pagamento.client.dto.update;

public class UpdateCartaoRequest {

    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}

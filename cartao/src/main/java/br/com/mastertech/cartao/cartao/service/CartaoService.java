package br.com.mastertech.cartao.cartao.service;

import br.com.mastertech.cartao.cartao.client.ClientDTO;
import br.com.mastertech.cartao.cartao.client.ClienteClient;
import br.com.mastertech.cartao.cartao.exception.CartaoAlreadyExistsException;
import br.com.mastertech.cartao.cartao.exception.CartaoNotFoundException;
import br.com.mastertech.cartao.cartao.model.Cartao;
import br.com.mastertech.cartao.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao create(Cartao cartao) {
        // Bloco de validação
        ClientDTO client = clienteClient.getById(cartao.getClientid());
        cartao.setClientid(client.getId());

        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());

        if(byNumero.isPresent()) {
            throw new CartaoAlreadyExistsException();
        }

        // Regras de negócio
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

}

package br.com.mastertech.cartao.cartao.client;


import br.com.mastertech.cartao.cartao.exception.ClienteOfflineException;

import java.io.IOException;

public class ClienteClientFallback implements ClienteClient {
    private Exception cause;

    ClienteClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public ClientDTO getById(Long id) {
        System.out.println(cause.getLocalizedMessage());
        if(cause instanceof RuntimeException) {
            throw new ClienteOfflineException();
        }
        throw (RuntimeException) cause;
    }

    @Override
    public ClientDTO create(ClientDTO clientDTO) {
        if(cause instanceof RuntimeException) {
            throw new ClienteOfflineException();
        }
        throw (RuntimeException) cause;
    }
}

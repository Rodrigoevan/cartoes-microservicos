package br.com.mastertech.cartao.cartao.client;

import br.com.mastertech.cartao.cartao.exception.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ClienteNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}

package br.com.mastertech.cartao.cartao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Microservico de Cliente não está disponivel")
public class ClienteOfflineException extends RuntimeException {
}

package br.com.mastertech.cartao.cartao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "cliente", url = "http://localhost:8080/", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    public ClientDTO getById(@PathVariable Long id);

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ClientDTO create(@RequestBody ClientDTO clientDTO);

}

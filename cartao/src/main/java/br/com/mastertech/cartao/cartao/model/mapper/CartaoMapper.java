package br.com.mastertech.cartao.cartao.model.mapper;

import br.com.mastertech.cartao.cartao.client.ClientDTO;
import br.com.mastertech.cartao.cartao.client.ClienteClient;
import br.com.mastertech.cartao.cartao.model.Cartao;
import br.com.mastertech.cartao.cartao.model.dto.create.CreateCartaoRequest;
import br.com.mastertech.cartao.cartao.model.dto.create.CreateCartaoResponse;
import br.com.mastertech.cartao.cartao.model.dto.get.GetCartaoResponse;
import br.com.mastertech.cartao.cartao.model.dto.update.UpdateCartaoRequest;
import br.com.mastertech.cartao.cartao.model.dto.update.UpdateCartaoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CartaoMapper {

    @Autowired
    private ClienteClient clienteClient;


    public  Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {
        System.out.println("clienteId(" + cartaoCreateRequest.getClienteId() + ")") ;
        ClientDTO client = clienteClient.getById(cartaoCreateRequest.getClienteId());

        client.setId(cartaoCreateRequest.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());

        cartao.setClientid(client.getId());
        return cartao;
    }

    public  CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteId(cartao.getClientid());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }

    public  Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public  UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setClienteId(cartao.getClientid());
        updateCartaoResponse.setAtivo(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public  GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setClienteId(cartao.getClientid());

        return getCartaoResponse;
    }

}
